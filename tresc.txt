Poniżej znajduje się treść trzeciego zadania zliczeniowego. Jest ona też dostępna w repozytorium w katalogu common/zadanie3.

-------------------------------------------------------------------

Programiści pracujący nad Bardzo Tajnym Systemem potrzebują klasy, która będzie
służyła do przechowywania danych o ograniczonym dostępie. Dokładniej: przy
tworzeniu obiektu klasy Sejf podajemy napis, który będzie przechowywany w
sejfie i liczbę dostępów do tego obiektu, na które zezwolimy. Należy założyć,
że napis może być długi i zadbać o jego efektywną obsługę przy tworzeniu
sejfu. Sejf powinien udostępniać następujące metody (w przykładach s1, s2 itd.
są zmiennymi lokalnymi typu Sejf):

Sejf(napis, liczba) -- tworzy Sejf z podanym napisem i maksymalną liczbą
  dostępów. Powinno być też możliwe storzenie sejfu z podaniem tylko
  napisu -- wtedy zezwalamy na 42 dostępy.

Dostęp do elementu -- powinien być możliwy przez standardowy operator []. Na
  przykład po utworzeniu sejfu s4 z napisem "abc123" wynikiem wyrażenia s4[3]
  powinien być znak '1' zwrócony jako liczba 16-bitowa ze znakiem. Jeżeli
  próbujemy dostać się do elementu, który nie istnieje (niepoprawny indeks) w
  napisie, to powinna być zwrócona wartość -1 i taka operacja nie liczy się
  jako próba odczytu. Po przekroczeniu liczby dozwolonych odczytów także
  powinna być zwracana wartość -1.

Manipulacje sejfem -- za pomocą operatorów +=, -=, *= można zmienić pozostałą
  liczbę prób dostępu. Na przykład po wykonaniu kodu

    Sejf s1("aaa", 2);
    s1[0];
    s1 *=3;

  będzie możliwe wykonanie jeszcze 3 prób dostępu do sejfu. W przypadku
  wyczerpania liczby prób dostępu należy uznać, że liczba przed operacją
  wynosi 0. Nie należy dopuszczać do ustawienia w sejfie pozostałej liczby prób
  dostępu na wartość ujemną. Ponadto w wyniku działania operatora -= pozostała
  liczba prób dostępu nie może się zwiększyć, a w wyniku działania operatorów
  += i *= pozostała liczba prób dostępu nie może się zmniejszyć.

Ze względów bezpieczeństwa nie pozwalamy na kopiowanie sejfów, ale powinno dać
się na przykład zamienić sejfy za pomocą operacji

    std::swap(s1, s2);

Kompilacja każdej z poniższych linii powinna zakończyć się błędem:

    Sejf s4 = s1;
    Sejf s5(s1);

kontroler() -- umożliwia dostęp do Kontrolera danego sejfu.
  Powinno być możliwe przeprowadzenie następującej operacji:

    os << s1.kontroler();

  która wypisze na strumień os napis:
  * "ALARM: WLAMANIE\n" jeżeli po wyczerpaniu dozwolonej liczby odczytów była
    co najmniej jedna próba odczytu (z poprawnym indeksem)
  * "ALARM: ZMANIPULOWANY\n" jeżeli na sejfie dokonano z sukcesem jednej z operacji +=, -=, *=
  * "OK\n" w pozostałych przypadkach
  Kontroler powinien pozostawać związany z sejfem przez cały czas, jeżeli oba
  te obiekty są widoczne. Na przykład wynikiem kodu:

    Sejf s1("aaa", 2);
    s1[2];
    auto k1 = s1.kontroler();
    cout << k1 << "test\n";
    s1[2];
    s1[3];
    s1[4];
    cout << k1;
    s1[3];
    cout << k1;
    s1[2];
    cout << k1;

  powinno być:

    OK
    test
    OK
    OK
    ALARM: WLAMANIE

  Kontroler powinno dać się zastosować jako wartość logiczną, przy czym
  wartość ta będzie prawdziwa, jeżeli liczba prób nie została jeszcze
  wyczerpana. Na przykład powinien działać następujący kod:

    Sejf s1("aaa", 2);
    if (s1.kontroler())
        /* można jeszcze odczytać */

  ale próba kompilacji kodu

    if (s1.kontroler() < s2.kontroler())
        ...;

  powinna zakończyć się błędem.

  Kontroler nie powinien udostępniać żadnych innych operacji.

  Kontroler można stworzyć tylko przez wywołanie metody kontroler() na
  Sejfie.

Sejf i Kontroler powinny być zabezpieczone (w zakresie, w którym jest to
możliwe w języku C++) przed modyfikacją ich stanu z zewnątrz.

-----------------------------------------------------------
Rozwiązanie

Rozwiązanie powinno składać się z plików sejf.h oraz sejf.cc. Można dodać też
pliki kontroler.h i kontroler.cc. Pliki te należy umieścić w repozytorium w
katalogu

grupaN/zadanie3/ab123456+cd123456

lub

grupaN/zadanie3/ab123456+cd123456+ef123456

gdzie N jest numerem grupy, a ab123456, cd123456, ef123456 są identyfikatorami
członków zespołu umieszczającego to rozwiązanie.
Katalog z rozwiązaniem nie powinien zawierać innych plików, ale może zawierać
podkatalog private, gdzie można umieszczać różne pliki, np. swoje testy. Pliki
umieszczone w tym podkatalogu nie będą oceniane.

 




PYTANIA I ODPOWIEDZI Z FORUM


 
Przemysław Gumienny
Odp: Zadanie 3
Przemysław Gumienny w dniu Tuesday, 5 November 2013, 18:30 napisał(a)
 

Jeżeli najpierw nastąpiło "włamanie", a potem "manipulacja" to wypisywać należy "włamanie" czy "manipulacja"?
 
Maciej Zielenkiewicz
Odp: Zadanie 3
Maciej Zielenkiewicz w dniu Tuesday, 5 November 2013, 20:19 napisał(a)
 

"Włamanie". W przypadku odwrotnej kolejności też należy wypisać "Włamanie". Włamanie jest zawsze ważniejsze od manipulacji.
 
Przemysław Gumienny
Odp: Zadanie 3
Przemysław Gumienny w dniu Tuesday, 5 November 2013, 20:48 napisał(a)
 

Czy po wykonaniu operacji
a) s1 += 0;
b) s1 -=0;
c) s1 *= 1;

powinna zostać ustawiona "manipulacja"? Są to poprawne operacje (bo w dodawaniu i mnożeniu ilość prób nie może się zmniejszyć, a w odejmowaniu zwiększyć), ale w sumie nic nie manipulujemy.

 
 
Maciej Zielenkiewicz
Odp: Zadanie 3
Maciej Zielenkiewicz w dniu Tuesday, 5 November 2013, 21:40 napisał(a)
 

Zgodnie z treścią zadania to jest manipulacja (bo spełnia podane warunki), więc powinna być za taką uważana.
 
Tomasz Stęczniewski
Odp: Zadanie 3
Tomasz Stęczniewski w dniu Wednesday, 6 November 2013, 15:26 napisał(a)
 

1. Czy kontroler powinien być związany z sejfem (czy ma wskazywać na ten sam sejf po wywołaniu swap)?

Tzn. co ma się wypisać po wykonaniu następującego kodu:

Sejf s1("aaa", 1), s2("bbb", 0);

auto k1 = s1.kontroler();

swap(s1, s2);

if(k1) cout << "k1 wciąż związany z sejfem z aaa" << endl;

else cout << "k1 związany z sejfem z bbb" << endl;

 

2. Co ma się stać po odwołaniu do kontrolera, którego sejf został usunięty?
 
Maciej Zielenkiewicz
Odp: Zadanie 3
Maciej Zielenkiewicz w dniu Wednesday, 6 November 2013, 16:38 napisał(a)
 

1. Kontroler ma być związany z sejfem, a nie jego zawartością. W podanym przykładzie wynikiem powinno być k1 związany z sejfem z bbb.

2. Cokolwiek, nie trzeba obsługiwać takiej sytuacji. W szczególności program może spowodować naruszenie ochrony pamięci (segmentation fault).
 
Paweł Adamowicz
Odp: Zadanie 3
Paweł Adamowicz w dniu Thursday, 7 November 2013, 13:15 napisał(a)
 

Czy po wykonaniu operacji -= mamy zmniejszyć ilość dostępów o tyle o ile jest możliwe póki nie dojdziemy do 0 czy jeśli dostajemy rozkaz zmniejszenia o więcej niż możemy to nic nie robimy?
 
Maciej Zielenkiewicz
Odp: Zadanie 3
Maciej Zielenkiewicz w dniu Thursday, 7 November 2013, 15:32 napisał(a)
 

Jeżeli nie jest możliwe zmniejszenie liczby dostępów o żądaną wartość to nic nie robimy.
 
Paweł Adamowicz
Odp: Zadanie 3
Paweł Adamowicz w dniu Friday, 8 November 2013, 16:46 napisał(a)
 
Kontroler można stworzyć tylko przez wywołanie metody kontroler() na  Sejfie. ale przeciez wywolanie auto k1 = s1.kontroler() wywola tez konstruktor kopiujacy kontrolera ktory w zwiazku z tym musi byc publiczny. co z tym fantem zrobic? mozna by zwracac const referencje ale trzeba wtedy inaczej uzyc auto co by bylo niezgodne z przykladem. mozna zwracac wskaznik ale wtedy inne rzeczy sie sypia.
 
Maja Zalewska
Odp: Zadanie 3
Maja Zalewska w dniu Friday, 8 November 2013, 20:08 napisał(a)
 

Czy w wyniku działania operatorów manipulacji liczba pozostałych prób może być zwększona ponad liczbę podaną jako argument w konstruktorze sejfu?
