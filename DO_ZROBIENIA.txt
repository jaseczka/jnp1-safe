1. Komentarze:
    * kontroler - Magda
    * sejf - Maja
2. Dodanie constów gdzie się da - Magda
3. Niemożność stworzenia sejfu z ujemnym access_left
4. Ograniczenie operatorów manipulacji, żeby nie dało się 
   wyskoczyć poza MAX_INT (albo inną sensowną stałą) - Magda
