#include "kontroler.h"

Kontroler::operator bool() const
{
    return safe.accessible();
}

bool Kontroler::brokenin() const
{
    return safe.breakin;
}

bool Kontroler::manipulated() const
{
    return safe.manipulation;
}

Kontroler::Kontroler(const Sejf& safe): safe(safe)
{
}

Kontroler::~Kontroler()
{
}

std::ostream &operator<<(std::ostream &stream, const Kontroler &k)
{
    if (k.brokenin()) {
        stream << "ALARM: WLAMANIE\n";
        return stream;
    }

    if (k.manipulated()) {
        stream << "ALARM: ZMANIPULOWANY\n";
        return stream;
    }
    
    stream << "OK\n";
    return stream;

}

