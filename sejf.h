#ifndef SEJF_H
#define SEJF_H

#include <climits>
#include <string>
#include <cstdint>
#include "kontroler.h"

/* domyślna liczba dostępów do sejfu */
static const size_t ACCESS_NUM = 42;
class Kontroler;

class Sejf
{
/*Sejf potrzebuje dostępu do prywatnych atrybutów kontrolera */
friend class Kontroler;

public:
	explicit Sejf(const std::string &secret, 
        size_t access_num = ACCESS_NUM);
    ~Sejf();
    Sejf(Sejf&& that); /* move constructor, potrzebny do poprawnego
						 działania std::swap(s1, s2) */
    Sejf& operator=(Sejf&& that); /* move operator - jak wyżej */
    int16_t operator[](size_t i); /* funkcja zwracająca 16bitową liczbe ze znakiem
									znajdującą się pod [i] miejscem w secret */
    void operator+=(size_t i);			/* pozwala zwiększyć liczbę dostępów */
    void operator-=(size_t i);			/* pozwala zmniejszyć liczbę dostępów */
    void operator*=(size_t i);			/* pozwala zwiększyć liczbę dostępów */
    Kontroler kontroler() const;	/* wywołanie funkcji kontrolera */

private:
    std::string secret; 			/* napis przechowywany w sejfie */
    size_t access_left;				/* liczba pozostałych dostępów */
    bool breakin;					/* czy nastąpiło włamanie do sejfu */
    bool manipulation;				/* czy liczba dostępów do sejfu została zmaniupulowana */ 
    Sejf(const Sejf& safe);			/* uniemożliwia kopiowanie sejfów */
    Sejf& operator=(const Sejf& safe); /* jak wyżej */
    bool accessible() const; 		/* czy można dokonać jeszcze jakiejś operacji na sejfie */
};

#endif

