#include <iostream>
#include <string>
#include <cstdint>
#include "sejf.h"

Sejf::Sejf(const std::string &secret, size_t access_num): 
secret(secret), access_left(access_num), 
breakin(false), manipulation(false)
{
}

Sejf:: ~Sejf()
{
}

Sejf::Sejf(Sejf&& that): secret(std::move(that.secret)), 
access_left(that.access_left), breakin(that.breakin),
manipulation(that.manipulation)
{
}

Sejf& Sejf::operator=(Sejf&& that)
{
    if (this != &that) {
        secret = std::move(that.secret);
        access_left = that.access_left;
        breakin = that.breakin;
        manipulation = that.manipulation;
    }
    return *this;
}

int16_t Sejf::operator[](size_t i)
{
    
    if (i >= secret.size())
        return -1;
    
    if (access_left == 0) {
        breakin = true;
        return -1;
    }

    --access_left;
    return static_cast<int16_t>(secret[i]);
}

void Sejf::operator+=(size_t i)
{
    if (access_left + i < access_left)
        return;
    manipulation = true;
    access_left += i;
}


void Sejf::operator-=(size_t i)
{
    if (access_left < i)
        return;
    if (access_left - i > access_left)
        return;
    manipulation = true;
    access_left -= i;
}

void Sejf::operator*=(size_t i)
{
    if (access_left * i < access_left)
        return;
    manipulation = true;
    access_left *= i;
}

   
bool Sejf::accessible() const
{
    return access_left > 0;
}

Kontroler Sejf::kontroler() const
{
    return Kontroler(*this);
}


