#include <climits>
#include <iostream>
#include "sejf.h"
#include <string>
#include <utility>

int main()
{
    Sejf uj("eee", -3);
    Sejf s1("aaa", 10);
    std::cout << "+= -5\n";
    s1 += -5;
    std::cout << "+= INT_MAX\n";
    s1 += INT_MAX;
    std::cout << "-= 11\n";
    s1 -= 11;
    std::cout << "-= -3\n";
    s1 -= -3;
    std::cout << "*= -1\n";
    s1 *= -1;
    std::cout << "*= INT_MAX\n";
    s1 *= INT_MAX;
/*
    Sejf s1("aaa", 10);
    Sejf s2("bbb", 30);
    std::cout << "s1: " << s1[0] << std::endl;
    std::cout << "s2: " << s2[0] << std::endl;
    auto k1 = s1.kontroler();
    auto k2 = s2.kontroler();
    s1 += 5;
    std::swap(s1, s2);
    std::cout << "k1: " << k1;
    std::cout << "k2: " << k2;
    std::cout << "s1: " << s1[0] << std::endl;
    std::cout << "s2: " << s2[0] << std::endl;
*/
/*
    std::cout << "TEST1\n";

    Sejf s1 ("aaa", 0);
    s1 += 0;
    s1[2];
    s1 += 2;
    s1[1];
    s1[3];

    auto k1 = s1.kontroler();
    std::cout << k1;

    s1[0];
    s1 += 3;
    s1[2];

    std::cout << s1.kontroler();

    std::cout << "TEST2\n";
    Sejf a("A", 1), b("B", 1);
    b += 1;
    auto k = b.kontroler();
    a[0]; a[0];
    std::swap(a, b);
    std::cout << k;
    
    std::cout << "TEST3\n";
    Sejf ujemny("ABC", -3);
    std::cout << ujemny.kontroler();
*/
    return 0;
}

