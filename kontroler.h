#ifndef KONTROLER_H
#define KONTROLER_H
#include <ostream>
#include "sejf.h"

class Sejf;

class Kontroler
{
    /* Sejf korzysta z prywatnego konstruktora Kontrolera. */
    friend class Sejf;
public:
    virtual ~Kontroler();
    /* C++11 safe-bool */
    explicit operator bool() const;
    /** Wypisuje stan sejfu związanego z Kontrolerem k.
     *  Możliwe wartości: "OK", "ALARM: WŁAMANIE", 
     *  "ALARM: ZMANIPULOWANY". Komunikat jest zakończony
     *  znakiem końca linii. Jeśli sejf jest jednocześnie
     *  zmanipulowany oraz nastąpiło włamanie (w dowolnej
     *  kolejności) wypisywany jest komunikat o włamaniu.
     */
    friend std::ostream& operator<<(std::ostream&, const Kontroler&);

private:
    /** Sejf z którym jest związany kontroler.
     */
    const Sejf& safe;
    /** Konstruktor do którego tylko Sejf ma dostęp.
     */
    explicit Kontroler(const Sejf& safe);
    /** Czy wystąpiło włamanie do sejfu, który jest
     *  związany z kontrolerem.
     */
    bool brokenin() const;
    /** Czy został zmanipulowany sejf, który jest 
     *  związany z kontrolerem.
     */
    bool manipulated() const;
};

#endif
