CXX = g++
CXXFLAGS = -Wall -std=c++11

ifeq ($(debug), 1)
	CXXFLAGS += -Werror -pedantic -DDEBUG -g
else
	CXXFLAGS += -O2
endif

.PHONY: all clean

all: test

test: sejf.o kontroler.o main.o
	$(CXX) $(CXXFLAGS) $^ -o $@

sejf.o: sejf.cc sejf.h kontroler.h

kontroler.o: kontroler.cc kontroler.h sejf.h

main.o: main.cc sejf.h kontroler.h

%.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -rf *.o
	
